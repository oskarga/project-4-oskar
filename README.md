# Project 4

**Skrevet av**: Oskar Gabrielsen
Basert på [prosjekt 3](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-61/prosjekt-3) av Hauk Aleksander Olaussen, Noran Baskaran og Oskar Gabrielsen.

## Prosjekt 4
Alternativ B har blitt valgt. Følgende har blitt gjort som del av prosjekt 4:
- Lagt til mulighet for å slette bruker i "ProfileView".
- Immplementert End-2-end testing gjennom cypress. Det var i påstått allerede implementert end-2-end testing i prosjekt 3, men denne testingen var i realiteten endpoint testing.
- Laget flere Jest tester for å oppnå høyere testdekningsgrad. Det er nå både Jest tester i frontend og backend.
- Lagt til escaping av query values i henhold til [mysqljs documentation](https://github.com/mysqljs/mysql#escaping-query-values).
- Forandret fra en enkelt database connection til connection pooling. Best practice. Dette resulterte i ganske store forandringer i QueryHandler klassen.
- Lagt til kontrollert shutdown av server. Dette gjør at SQL serveren får lukket aktive connections mot node serveren.
- Lagt til "LIMIT 1" på flere mysql queries. Øker ytelse og er en failsafe mot at flere rader blir påvirket av query.

I tillegg til denne README.md filen ligger det en README.md i både frontend og backend mappene. Disse er i stor grad like som README.md filene fra prosjekt 3 da mye av dokumentasjonen fra prosjekt 3 fortsatt er relevant. Det er dog gjort modifikasjoner for å samsvare med forandrigene gjort som del av prosjekt 4.

Nedenfor følger framgangsmåte for å installere prosjektet, kjøre prosjektet og kjøre tester. Framgansmåten er kopiert fra prosjekt 3, med noen små forandringer.

## Hvordan sette opp og kjøre prosjektet

Prosjektet som helhet bruker [Node.js](https://nodejs.org/).
Det er allikevel to ulike applikasjoner som begge må kjøres separat for at prosjektet skal kjøres slik det er ment. Disse er delt inn i [frontend](./frontend) og [backend](./backend).

### Installasjon
For å installere det du trenger av avhengigheter må du kjøre disse kommandoene i både **frontend** og **backend**. Det kan lønne seg å åpne to terminaler allerede nå, ettersom du kommer til å trenge disse når du skal kjøre prosjektet.

For **frontend**:
```sh
$ cd frontend
$ npm install
```

For **backend**:
```sh
$ cd backend
$ npm install
```
### Kjøring
Det er viktig at du er koblet opp til NTNU nettet via VPN eller direkte for at applikasjonen skal fungere ettersom databasen er hostet på NTNU sine servere.

Etter du har installert det som trengs av avhengigheter med kommandoene over, og er tilkoblet NTNU-nettet, så er du klar for å kjøre prosjektet. Du må kjøre både **frontend** og **backend** hver for seg - og siden du allerede har to terminaler oppe, så er dette veldig enkelt.

I sine respektive terminaler, skriver du:
```sh
$ npm start
```
Dette vil starte NodeJS applikasjonen som ligger i den valgte mappen i terminalen. Har du gjort dette riktig, vil du se dette i terminalene. 

Output **frontend**:

![Fontend output](./images/frontend.PNG "Frontend output")

Output **backend**: 

![Backend output](./images/backend.PNG "Backend output")

Frontend vil kjøre på port 3000 - [klikk her for å åpne frontend](http://localhost:3000) 

Backend vil kjøre på port 5000  -  [klikk her for å åpne backend](http://localhost:5000)

## Testing
Cypress testene og noen Jest tester er avhenginge av at applikasjonen kjører(både frontend og backend). Det bør derfor åpnes et tredje terminal vindu for å kjøre testene. Det ligger både Jest tester i frontend og backend mappene.

Jest testene i frontend kan kjøres vi følgende kommandoer:
```sh
$ cd frontend
$ npm run test
```

Jest testene i backend kan kjøres vi følgende kommandoer: 
```sh
$ cd backend
$ npm run test
```

Cypress testene kan kjøres vi følgende kommandoer:
```sh
$ cd frontend
$ npx cyprex open
```
 

## Database
Databasen kjører på en av NTNU sine virituelle maskiner. Det er en MySQL database bestående av informasjon om over 15 tusen byer fra rundt om i verden - samt informasjon om brukere som har registrert seg på siden. Byene har denne strukturen,
```ts
interface City {
  name: string;
  name_native: string;
  country: string;
  isCapital: boolean;
  iso2: string;
  iso3: string;
  population: number;
  lat: number;
  lng: number;
  id: number;
}
```

mens brukerene har denne strukturen.

```ts
interface User {
  username: string;
  password: string;
  age: number;
  country: string;
  city: string;
  favourite_city: string;
  favourite_country: string;
}
```

Under kan du se informasjonen for å koble deg opp til databasen.
```ts
const DATABASE_INFO = {
    HOST: it2810-61.idi.ntnu.no,
    DATABASE: prosjekt3,
    USER_NAME: prosjekt3,
    USER_PASSWORD: password,
    PORT: 3306,
}
```
Dette kan du gjøre eksempelvis i MySQL workbench eller dynamisk i koden ved bruk av [mysql npm pakken](https://www.npmjs.com/package/mysql). Det går også an å logge seg inn for å se databasen dersom du har tilgang til en NTNU bruker.

License
----
[MIT Lisence](./LICENSE)



  

