# Project 4
**Skrevet av**: Oskar Gabrielsen
Basert på [prosjekt 3](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-61/prosjekt-3) av Hauk Aleksander Olaussen, Noran Baskaran og Oskar Gabrielsen.

## Backend
Denne teksten skal gi en innføring i hvordan backend delen av prosjektet fungerer, samt diskutere rundt valg som er gjort i løpet av utviklingen. Selve frontend delen er skrevet i [TypeScript](https://www.typescriptlang.org/) ved bruk av biblioteket [Express](https://expressjs.com/). Express er et bibliotek som gjør det enkelt å lage et API i NodeJS.

### Oppsett og struktur
Det er kun fire .ts filer som inngår i backend modulen. 
Disse er:
- server/**quieries.ts**
- server/**QueryHandler.ts**
- server/**Server.ts**
- **index.ts**

##### server/queries.ts
Denne filen inneholder de ulike SQL queriene som blir brukt for å hente og skrive data til databasen. Disse tar inn ulike parametre som eksempelvis brukernavn, bynavn m.m. for å spesifisere hva vi er ute etter i databasen.

##### server/QueryHandler.ts
Denne klassen kobler seg til databasen som skal brukes ved bruk av innloggings-legitimasjonen som ligger i .env filen. Det hender at vi får en timeout av databasen dersom API-et har vært stillestående for lenge, og dermed så har vi en timeout på 2 sekunder dersom det skjer før vi kobler til databasen på nytt. Dette skjer i *handleDisconnect()* metoden. Hovedmetoden i denne klassen er *sendQuery()* metoden som tar inn querien som skal sendes til databasen, og returnerer det databasen gir tilbake på JSON format. Denne metoden blir kalt i **Server.ts** og querien er avhengig av hvilket endpoint og metode på HTTP requesten brukeren spesifiserte.

##### server/Server.ts
Denne klassen spesifiserer de ulike enpointene som kan aksesseres av en eventuell bruker.
Disse er:

GET
- **http://localhost:|port|/**

    Returnerer alle byer

- **http://localhost:|port|/city/|cityname|/**

    Returnerer aller byer med det gitte navnet

- **http://localhost:|port|/country/|country|/**

    Returnerer alle byer i det gitte lander

- **http://localhost:|port|/users/**

    Returnerer alle brukere

- **http://localhost:|port|/user/|username|/**

    Returnerer brukeren med det gitte brukernavnet

POST
- **http://localhost:|port|/login/**

    Sjekker om den gitte brukeren eksisterer og er riktig

- **http://localhost:|port|/adduser/**

    Legger til den nye brukeren dersom kravene er møtt

DELETE
- **http://localhost:|port|/removeuser/|username|/**

    Sletter brukeren med det gitte brukernavnet

Denne informasjonen er også kommentert i koden.

Serveren får en express app og port fra **index.ts** som den bruker når den startes opp for å lytte etter requests.

##### index.ts
Dette er filen som kjøres av npm når vi kjører 
```sh
$ npm start
```
Den initialiserer en instanse av Server klassen med en gitt konfigurasjon av en Express app, samt en port å lytte på - som i vårt tilfelle er hardkodet til port 5000.

### Valg av komponenter og fremgangsmåter

#### Eksterne pakker og komponenter

##### [ts-node](https://www.npmjs.com/package/ts-node)
Denne pakken gjør det mulig å kjøre TypeScript(.ts) filer direkte uten å måtte gjøre de om til JavaScript(.js) først, noe som er veldig kjekt i dette tilfellet.

##### [dotenv](https://www.npmjs.com/package/dotenv)
Denne pakken gjør det mulig å lage egne environment variabler lagret i **.env** filen, for å så aksessere dem senere. I dette tilfellet er det kanskje ikke så mye vits ettersom filen ikke er skjult, men allikevel en god pakke å ha i en production setting.

##### [Express](https://www.npmjs.com/package/express)
Dette er hovedpakken i backend delen. Express biblioteket gjør det mulig å enkelt lage et REST API med TypeScript/JavaScript, og er noe alle gruppemedlemmene har vært borti før. Det var også en av de anbefalte måtene å gjøre det på.

##### [Mysql](https://www.npmjs.com/package/mysql)
Denne pakken inneholder funksjonaliteten vi brukte for å koble til MySQL databasen som er forklart mer i [rot-readme](../README.md). Den gjør det mulig å koble til og sende queries, for å så få tilbake denne dataen ved bruk av TypeScript/JavaScript.

##### [Bcryptjs](https://www.npmjs.com/package/bcryptjs)
Denne pakken inneholder funksjonaliteten vi brukte for å hashe passord før de lagres i databasen, samt å sjekke om gitte passord i login er compatible med hashen i databasen.

#### Fremgangsmåter
Vi hasher passordene når brukeren registrerer seg for mer sikker lagring av brukerdata. Dette gjøres - som beskrevet over - med pakken [bcryptjs](https://www.npmjs.com/package/bcryptjs).

Ved å bruke URL-parametre for å få ulike byer basert på navn eller land så gjør det det enkelt å få ut få enheter av et stort datasett. Det er derfor vi brukte dette i GET requestene.

Når en bruker skal logge inn sjekker metoden av brukeren som ble gitt i som data i POST requesten eksisterer i databasen, og at passordet stemmer. Dersom det gjør dette returneres brukeren på JSON format slik at React frontend delen kan logge brukeren inn.

### Testing
Det er ingen egne tester for backend ettersom det ble sagt at dette ikke er nødvendig. API-et blir allikevel testet godt kjennom end-to-end testene som kjøres i [**frontend**](../frontend) ved å teste ulike endpoints og hva de returnerer.











  

