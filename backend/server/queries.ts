import mysql from "mysql";
// This file holds the different SQL queries called by the API to fetch data from the database

export const SELECT_ALL = (): string => {
  return "SELECT * FROM cities LIMIT 7000;";
};

export const SELECT_CITY_BY_NAME = (cityName: string): string => {
  return `SELECT * FROM cities WHERE name = ${mysql.escape(cityName)};`;
};

export const SELECT_CITIES_BY_COUNTRY = (country: string): string => {
  return `SELECT * FROM cities WHERE country = ${mysql.escape(country)};`;
};

export const SELECT_USER = (username: string): string => {
  return `SELECT * FROM users WHERE username = ${mysql.escape(username)} LIMIT 1;`;
};

export const SELECT_ALL_USERS = (): string => {
  return `SELECT * FROM users;`;
};

export const DELETE_USER = (username: string): string => {
  return `DELETE FROM users WHERE username = ${mysql.escape(username)} LIMIT 1;`;
};

export interface User {
  username: string;
  password: string;
  age: number;
  country: string;
  city: string;
  favourite_city: string;
  favourite_country: string;
}

export const CREATE_NEW_USER = (user: User): string => {
  return `INSERT INTO users (username, password, age, country, city, favourite_city, favourite_country) 
  VALUES (
    ${mysql.escape(user.username)},
    ${mysql.escape(user.password)},
    ${mysql.escape(user.age)},
    ${mysql.escape(user.country)},
    ${mysql.escape(user.city)},
    ${mysql.escape(user.favourite_city)},
    ${mysql.escape(user.favourite_country)});`;
};
