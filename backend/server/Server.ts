import { Express, Request, Response, json } from "express";
import QueryHandler from "./QueryHandler";
import bcrypt from "bcryptjs";
import {
  User,
  SELECT_ALL,
  SELECT_CITY_BY_NAME,
  SELECT_CITIES_BY_COUNTRY,
  SELECT_USER,
  SELECT_ALL_USERS,
  CREATE_NEW_USER,
  DELETE_USER,
} from "./queries";

/**
 * This class is the hold the api logic
 */
export class Server {
  private app: Express;
  private queryHandler: QueryHandler;

  /**
   * It takes in an express app and saves this in the app variable
   * It also creates a QueryHandler and adds the different routes.
   * @param app
   */
  constructor(app: Express) {
    this.app = app;
    this.queryHandler = new QueryHandler();
    this.routes();
  }

  /**
   * This method adds all the routes concerning cities to the app.
   */
  private setCitiesRoutes = () => {
    // Path: localhost:<port>/ | Returns all the cities
    this.app.get("/", async (req: Request, res: Response) => {
      const data: string = await this.queryHandler.sendQuery(SELECT_ALL());
      res.header("Content-Type", "application/json");
      res.send(data);
    });
    // Path: localhost:<port>/city/<cityname> | Returns all the cities with the given cityname
    this.app.get("/city/:cityName", async (req: Request, res: Response) => {
      const cityName: string = req.params.cityName;
      const data: string = await this.queryHandler.sendQuery(
        SELECT_CITY_BY_NAME(cityName)
      );
      res.header("Content-Type", "application/json");
      res.send(data);
    });

    // Path: localhost:<port>/country/<countryname>/ | Returns all the cities in the given country
    this.app.get("/country/:country", async (req: Request, res: Response) => {
      const country = req.params.country;
      const data: string = await this.queryHandler.sendQuery(
        SELECT_CITIES_BY_COUNTRY(country)
      );
      res.header("Content-Type", "application/json");
      res.send(data);
    });
  };

  /**
   * This method adds all the routes concerning users
   */
  private setUsersRoutes = () => {
    // Path: localhost:<port>/users | Returns all the users
    this.app.get("/users", async (req: Request, res: Response) => {
      const data: string = await this.queryHandler.sendQuery(
        SELECT_ALL_USERS()
      );
      res.header("Content-Type", "application/json");
      res.send(data);
    });

    // Path: localhost:<port>/users/<username> | Returns the user with the given username
    this.app.get("/user/:username", async (req: Request, res: Response) => {
      const username = req.params.username;
      const data: string = await this.queryHandler.sendQuery(
        SELECT_USER(username)
      );
      res.header("Content-Type", "application/json");
      res.send(data);
    });

    this.app.post("/login/", async (req: Request, res: Response) => {
      const username: string = req.body.user.username;
      const password: string = req.body.user.password;
      const result: string = await this.queryHandler.sendQuery(
        SELECT_USER(username)
      );
      const json = JSON.parse(result);
      if (json.length > 0 && bcrypt.compareSync(password, json[0].password)) {
        res.header("Content-Type", "application/json");
        res.statusCode = 200;
        // TO DO: Send back session cookie
        // res.cookie =
        res.send(json[0]);
      } else {
        res.header("Content-Type", "application/json");
        res.statusCode = 200;
        res.send({ status: 401 });
      }
    });

    this.app.post("/adduser/", async (req: Request, res: Response) => {
      const user: User = req.body.user;
      const validCharsCredentials: RegExp = new RegExp(
        "^[a-zA-Z0-9!@£$#%&= ]+$"
      );
      const validCharsInfo: RegExp = new RegExp("^$|^[a-zA-Z0-9!@£$#%&= ]+");
      if (
        user.username.length > 6 &&
        user.username.length < 30 &&
        user.password.length > 6 &&
        user.password.length < 30 &&
        user.age > 5 &&
        user.age < 140 &&
        validCharsCredentials.test(user.username) &&
        validCharsCredentials.test(user.password) &&
        validCharsInfo.test(user.city) &&
        validCharsInfo.test(user.country) &&
        validCharsInfo.test(user.favourite_city) &&
        validCharsInfo.test(user.favourite_country)
      ) {
        user.password = bcrypt.hashSync(user.password, 10);
        const result: string = await this.queryHandler.sendQuery(
          CREATE_NEW_USER(user),
          false
        );
        res.header("Content-Type", "application/json");
        res.send(result);
      } else {
        res.header("Content-Type", "application/json");
        res.send(false);
      }
    });

    this.app.delete(
      "/removeuser/:username",
      async (req: Request, res: Response) => {
        const username = req.params.username;
        const result: string = await this.queryHandler.sendQuery(
          DELETE_USER(username),
          false
        );
        res.header("Content-Type", "application/json");
        res.send(result);
      }
    );
  };

  /**
   * Adds all the routes, both for cities and users
   */
  private routes = () => {
    this.setCitiesRoutes();
    this.setUsersRoutes();
  };

  /**
   * Starts the app at the given port
   * @param port - port to run the server
   */
  public start(port: number): void {
    const server = this.app.listen(port, () => {
      console.log(`No errors | Starting server!`);
      console.log();
      console.log(`Server listening on port ${port}! CTRL + C to shutdown server.`);

      //Shuts the server down upon recieving SIGINT signal (CTRL + C)
      process.on('SIGINT', () => {
        console.info('SIGINT signal received. Please wait for the server to gracefully shut down.');
        server.close(() => {
          console.log('Express http server has shut down.');
          this.queryHandler.closeAllConnections();
          console.log('Database connections closed');
          process.exit(0);
        });
      });
    });
  }
}
