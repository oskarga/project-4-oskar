import mysql, { Connection } from "mysql";

/**
 * The QueryHandler class connects to the database and sends the given query
 * to the database.
 */
class QueryHandler {

  //Creates a connection pool instead of a single connection
  private pool: mysql.Pool;

  /**
   * The constructor creates a connection pool
   */
  constructor() {
    this.pool = mysql.createPool({
      connectionLimit : 50,
      host            : process.env.DB_HOST,
      user            : process.env.DB_USER,
      password        : process.env.DB_PASSWORD,
      database        : process.env.DB_NAME,
      waitForConnections: true
    });
  }

  /**
   * This method sends a given query to the database and returns the data in JSON format
   * @param query - The given query
   * @param get - Whether its a GET request or not | Defaults to true
   */
  public sendQuery = async (
    query: string,
    get: boolean = true
  ): Promise<string> => {
    try {
      // The data is a JSON formatted string of the object fetched from the database
      const data: string = await new Promise<string>((resolve, reject) => {
        this.pool.query(query, async (err, rows) => {
          if (err) reject(err);
          // Only prettify the data if the request is a GET request
          if (get) this.prettify(rows);
          // JSON format the rows returned
          resolve(JSON.stringify(rows, null, 2));
        });
      });
      return data;

    } catch (err: any) {
      // If the user uses a POST request to make a user with an already taken username, the POST will fail and
      // return 'false. If another error occured, another string will be returned
      if (err.code == "ER_DUP_ENTRY") {
        return `false`;
      }
      return err.code + " "+err.message + "Could not make a user with those parameters";
    }
  };

  /**
   * Helper method for making 1 to true and 0 to false for JSON boolean purposes
   * @param cities - array of cities to prettify
   */
  private prettify = (cities: any) => {
    for (let city of cities) {
      city.isCapital = city.isCapital ? true : false;
    }
  };

  public closeAllConnections = () =>{
    this.pool.end();
  };
}

export default QueryHandler;
