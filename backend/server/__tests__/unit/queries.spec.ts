import{
    User,
    SELECT_ALL,
    SELECT_CITY_BY_NAME,
    SELECT_CITIES_BY_COUNTRY,
    SELECT_USER,
    SELECT_ALL_USERS,
    CREATE_NEW_USER,
    DELETE_USER,
  } from "../../queries";

describe("SELECT_ALL()", () => {
    it("Should select all cities limited by 700", () => {
        const result: string = SELECT_ALL();
        expect(result).toEqual("SELECT * FROM cities LIMIT 7000;");
    });
});

describe('SELECT_CITY_BY_NAME("London")', () => {
    it("Should return query with London", () => {
        const result: string = SELECT_CITY_BY_NAME("London");
        expect(result).toEqual("SELECT * FROM cities WHERE name = 'London';");
    });
});

describe('SELECT_CITY_BY_NAME(")', () => {
    it("Should escape strings in return query", () => {
        const result: string = SELECT_CITY_BY_NAME('"');
        expect(result).toEqual("SELECT * FROM cities WHERE name = '\\\"';");
    });
});

describe('SELECT_CITIES_BY_COUNTRY(")', () => {
    it("Should escape strings in return query", () => {
        const result: string = SELECT_CITIES_BY_COUNTRY('United Kingdom');
        expect(result).toEqual("SELECT * FROM cities WHERE country = 'United Kingdom';");
    });
});

describe('SELECT_USER("testuser123")', () => {
    it("Should return a valid SQL query with the right parameters", () => {
        const result: string = SELECT_USER("testuser123");
        expect(result).toEqual("SELECT * FROM users WHERE username = 'testuser123' LIMIT 1;");
    });
});

describe('SELECT_ALL_USERS()', () => {
    it("Should return a valid SQL query with the right parameters", () => {
        const result: string = SELECT_ALL_USERS();
        expect(result).toEqual("SELECT * FROM users;");
    });
});
