import { Server } from "./server/Server";
import express from "express";
import dotenv from "dotenv";
import cors from "cors";

// Makes it possible to use environment variables
dotenv.config();

// new Express app
const app = express();
// Adds the different plugins to the app
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Starts the server at the given port
const port = 5000;
const server = new Server(app);
server.start(port);
