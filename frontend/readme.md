# Prosjekt 4
**Skrevet av**: Oskar Gabrielsen
Basert på [prosjekt 3](https://gitlab.stud.idi.ntnu.no/it2810-h20/team-61/prosjekt-3) av Hauk Aleksander Olaussen, Noran Baskaran og Oskar Gabrielsen.

## Frontend
Denne teksten skal gi en innføring i hvordan frontend delen av prosjektet fungerer, samt diskutere rundt valg som er gjort i løpet av utviklingen. Selve frontend delen er skrevet i [TypeScript](https://www.typescriptlang.org/) ved bruk av rammeverket [ReactJS](https://reactjs.org/). Som statehandling brukes [Redux](https://redux.js.org/), som er et rammeverk som sentraliserer applikasjonens tilstand mens den kjører. Vi har kommentert alle metoder og klasser direkte i koden slik at gjennomgang og forståelse av koden skal være så enkel som mulig - så noe av det som skrives her gjentas antageligvis der.

### Oppsett og struktur
Når vi skulle dele opp de ulike delene av frontenden så ville vi gjøre dette på en så oversiktlig måte som mulig. Dette oppnår vi med å ha fire ulike mapper som inneholder de ulike delene av koden (vi ser bortifra rot-src og test mappen). Under er en beskrivelse av de ulike mappene, og filene som er inneholdt i dem, samt en beskrivelse av **App.tsx** som er hovedkomponenten for applikasjonen.

- src/**actions**
- src/**api**
- src/**components**
- src/**store**
- src/**App.tsx**

##### Actions
I actions mappen ligger det klasser som definerer de ulike typene av actions som kan bli dispatchet i løpet av applikasjonsens livssyklus - disse er **CityActionsTypes.ts** og **LoginActionsTypes.ts**. De to andre klassene i mappen definerer de faktiske dispatchene som kalles i komponentklassene ved bruk av *useDispatch()* fra Redux. 

##### Api
Denne mappen inneholder kun én klasse - **Controller.ts**. Denne klassen inneholder alt av HTTP requests som sendes til apiet. For å sende requestene bruker vi npm pakken [axios](https://www.npmjs.com/package/axios). Denne pakken gjør det enkelt å sende ulike typer av HTTP requests, noe vi trenger da vi både leser og skriver data til databasen. Hva de ulike metodene gjør er kommentert direkte i koden.

##### Components
Dette er hovedmappen i prosjektet da den inneholder alt som er av *.tsx* komponenter som brukes og vises på nettsiden. I denne mappen har vi:

- **HomePage.tsx**: som tar inn en prop *cities* som er en liste med byer som skal vises i tabellen på siden.
- **HomePageHeader.tsx**: som endrer seg basert på om en bruker er logget inn eller ikke. Dersom en bruker er logget inn vil knappen oppe til høyre endre seg til å vise profil, i stedet for å ta brukeren til en login-side.
- **LoginPage.tsx**: som håndterer innlogging og registrering av brukeren. Denne inneholder en form fra pakken [Formik](https://formik.org/docs/overview) dersom brukeren ikke er logget inn. Dersom brukeren er logget inn derimot, så vises **ProfileView.tsx** i stedet.
- **ProfileView.tsx**: som viser informasjonen som brukeren har gitt ved registrering. Denne informasjonen lagres på databasen, og hentes igjen når brukeren logger inn. Bruker har ikke mulighet til å endre dette. Denne siden vil vise informasjonen, og et kart over favorittbyen dersom denne er gitt *OG* eksisterer i datasettet som hentes fra API-et. Hvis byen ikke eksisterer, vises kun informasjonen som er gitt ved registrering. For å få tak i brukeren brukes *useSelector()* metoden fra Redux.
- **table/DataTable.tsx**: som tar inn byene som skal vises for å vise disse i en tabell (table). Denne tabellen er interaktiv med mulighet for filtrering, sortering og søk. Dersom brukeren klikker på en rad i tabellen vil mer informasjon om byen, samt et kart over den vises. Dette kartet defineres i **InitMap.tsx**. Tabellen er en komponent fra npm pakken [react-bootstrap-table-next](https://www.npmjs.com/package/react-bootstrap-table-next), som gjør det mulig å implementere de ulike interaktive metodene for tabellen svært enkelt. 
- **table/InitMap.tsx**: som viser den utvidede informasjonen som vises når man klikker på en rad i tabellen. Den bruker pakken [google-map-react](https://www.npmjs.com/package/google-map-react) for å vise kartet basert på lenge- og bredde-gradene til byene. 
- **table/utils/constants.tsx**: som inneholder konstanter som brukes for å utforme og definere tabellen. Disse konstantene viser strukturen på de ulike kolonnene i tabellen, samt hvilke kolonner det skal være mulig å sortere, søke og filtrere m.m.

##### Store
Denne mappen inneholder og konfigurerer Redux håndteringen i applikasjonen. Dette inkluderer både store og reducers. Vi har to reducers (**CitiesReducer.ts** og **LoginReducer.ts**) som blir kombinert ved bruk av Redux-metoden *combineReducers()* for å lage én **RootReducer.ts** som importeres og brukes i **store.ts**. Storen bruker mellomleddene *thunk* (for asynkron kode) og *redux-logger* (for oversiktlig konsoll-logging) for å øke utviklingshastighet og kvalitet.

##### App.tsx
Som i de fleste andre ReactJS applikasjoner er **App.tsx** hovedkomponenten som kobler de andre komponentene sammen. Ved loading av denne siden, dispatcher vi 
*LoadAllCities()* som kaller på API-et, og henter alle byene. Vi bruker Redux metoden *useSelector()* for å få tak i disse byene fra storen, samt en eventuell bruker som senere logger inn eller registrerer seg. Dersom *login* boolean-en er *true*, vil login/profil siden vises, ellers vil tabellen med byer vises. Headeren endres ved at *loggedIn* boolean-en er *true* eller *false*.

### Valg av komponenter og fremgangsmåter

#### Eksterne pakker og komponenter

##### [React-bootstrap-table next](https://www.npmjs.com/package/react-bootstrap-table-next)
Dette er så klart hoveddelen av nettsiden - den faktiske tabellen med byer som vises. Vi valgte å bruke denne pakken ettersom den minimalistiske stilen, samt enkelheten av å tilpasse den til sitt eget er svært enkelt. Den kommer allerede med inenbygde søke, sorterings og filtreringsmuligheter, noe som gjør det svært enkelt å navigere.

##### [Axios](https://www.npmjs.com/package/axios)
Dette er en velkjent pakke som alle på gruppen hadde kjennskap til fra før. Den gjør det som sagt enkelt å sende ulike typer HTTP requests (GET, POST og DELETE for våre behov) - og var en no-brainer for oss.

##### [Google-map-react](https://www.npmjs.com/package/google-map-react)
Ettersom vi hadde data om lengde- og bredde-grader så visste i tidlig at vi hadde lyst til å vise et kart over byen som ekstra data. Dette fikk vi til med bruk av denne pakken, som kun trenger en Google API nøkkel, som står i plaintekst i koden, samt koordinatene til byen som skal vises. Vi valgte denne ettersom den gjorde denne funksjonaliteten enkel å oppnå.

##### [Formik](https://formik.org/)
Denne pakken inneholder funksjonaliteten vi brukte for innloggings- og registrerings-formen. Å lage og style en form har vi opplevd er ganske tidskrevende, og ettersom det eksisterer rammeverk som gjør det enklere, så brukte vi formik - som er et kjent rammeverk for å lage forms i React.

##### Andre pakker
Vi har også noen pakker som er spesifike for testing - slik som [enzyme](https://www.npmjs.com/package/enzyme), [enzyme-adapter-react-16](https://www.npmjs.com/package/enzyme-adapter-react-16) og [react-test-renderer](https://www.npmjs.com/package/react-test-renderer) m.m. Disse pakkene er installert kun fordi det gjør testing enklere og mer oversiktlig - og ikke minst bedre.

#### Fremgangsmåter
For å endre side til login/registrerings siden har vi en boolean *login* i storen som settes til *true* eller *false* når man klikker på knappen oppe til høyre i headeren. Dersom man er på login/registrerings-siden vil den settes til *false* ved klikk, og dermed gå tilbake til å vise tabellen - og vice verca.

For å vise favorittbyen til en innlogget bruker sjekker vi om byen som brukeren har skrevet inn eksisterer i datasettet som ligger i storen. Dersom den gjør det vises byen som et kart på profilsiden, hvis den ikke eksisterer (dette kan være fordi byen ikke er i datasettet eller at brukeren har en tom string som favorittby) så vil vi kun vise informasjonen som brukeren skrev inn ved registrering. 

Vi bruker konsekvent *useSelector()* for å aksessere objektene i store. Det er en enkel metode som fungerer godt.

Vi bruker konsekvent *useDispatch()* og *dispatch()* for å dispatche en action.

### Testing
Vi har en blanding av snapshot, endpoint, end-2-end og UI-tester som tester ulike deler av applikasjonen. Til sammen utgjør dette 68 testcaser fordelt over 8 suites som alle ligger i testmappen i **src**

- Snapshot testene tester ikke annet enn at komponenten som testes er konsekvent i måten den er strukturert på hver gang den loades. 
- Endpoint testene tester at man får dataene man forventer når man kaller API-et. Så klart krever dette at API-et faktisk er online og lytter etter requests, hvis ikke vil testene feile. Disse testene kjører funksjoner som sender en HTTP request (**Controller.tsx**) til API-et, og venter på svaret for å så teste dette. Eksempelvis kan dette være å prøve å hente en bruker som eksisterer eller prøve å hente en bruker som ikke eksisterer for å så sjekke at metodene returnerer det de skal.
- UI-testene tester at funksjonaliteten som er implementert i ulike interaktive elementer som eksempelvis knapper på nettsiden kjører og fungerer som de skal.
- End-2-end testene er laget med cypress og tester applikasjonen som helhet.

For å kjøre testene som en helhet:
```sh
$ npm test
```

For å kjøre testene med testdekningene:
```sh
$ npm run test:coverage
```
Cypress testene må kjøres separat med:
```sh
$ npx cypress open
```
#### Testdekning
Vi har en god testdekning på totalt ca. 87%. Under kan du se en oversikt over testdekningen av de ulike filene og klassene i prosjektet.
![Testdekning](../images/coverage.PNG "Testdekning")










  

