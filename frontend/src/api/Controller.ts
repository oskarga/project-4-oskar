import axios, { AxiosResponse } from "axios";

const baseURL = "http://localhost:5000";

/**
 * Sends a HTTP GET request to the API with the axios package.
 * @param url - url to fetch
 */
export const fetchFromAPI = async (url: string): Promise<City[]> => {
  const response: AxiosResponse = await axios.get(url);
  if (response.status === 200) {
    return response.data;
  }
  return [];
};

/**
 * Sends a HTTP GET request to the API to fetch a list of cities with the given name
 * @param city - the name of the city
 */
export const fetchSortByCityName = async (city: string): Promise<City[]> => {
  if (city === "") {
    return [];
  }
  const url: string = `${baseURL}/city/${city}`;
  return await fetchFromAPI(url);
};

/**
 * Sends a HTTP GET request to the API to fetch a list of cities with the given country
 * @param country - the name of the country
 */
export const fetchSortByCountry = async (country: string): Promise<City[]> => {
  if (country === "") {
    return [];
  }
  const url: string = `${baseURL}/country/${country}`;
  return await fetchFromAPI(url);
};

/**
 * Sends a HTTP GET request to the API to fetch all cities
 */
export const fetchAllCities = async (): Promise<City[]> => {
  return await fetchFromAPI(baseURL);
};

/**
 * Send a HTTP POST request to the API with the given user as the body.
 * Will recieve either the user if all went fine, or an error/false if something went wrong.
 * @param user - the user to add
 */
export const addNewUser = async (user: User) => {
  const url: string = `${baseURL}/adduser/`;
  const res: any = await axios({
    method: "post",
    url,
    data: {
      user,
    },
  });
  return res.data;
};

export const validateUser = async (user: User) => {
  const url: string = `${baseURL}/login/`;
  const res = await axios({
    method: "post",
    url,
    data: {
      user,
    },
  })
  console.log(res.data);
  return res;
};

/**
 * This method removes a user from the database by providing the correct
 * username.
 * @param username - the username of the user to be removed
 */
export const removeUser = async (username: string) => {
  const url: string = `${baseURL}/removeuser/${username}`;
  const res: any = await axios.delete(url);
  return res;
};
