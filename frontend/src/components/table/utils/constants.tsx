import React, { CSSProperties } from "react";
import { ExpandRowProps } from "react-bootstrap-table-next";
import { selectFilter, SelectFilterOptions, textFilter } from "react-bootstrap-table2-filter";
import InitMap from '../InitMap'

// This object is used as the options for the isCapital filter-dropdown.
const isCapitalSelectOptions: SelectFilterOptions = {
  true: "true",
  false: "false",
};

/**
 * This method will change the color of the text in the isCapital column based on if the city
 * is indeed a capital in the country. Green if true, red if false.
 * @param row - the table row
 */
const isCapitalFormatter = (row: boolean) => {
  const capitalStyle: CSSProperties = {
    color: row ? "green" : "red",
  };
  return <span style={capitalStyle}>{row.toString()}</span>;
};

/**
 * Not all cities have a population from the database. They are defaulted to 0.
 * Instead of showing 0, we show UNKNOWN in red text.
 * @param row - the table row
 */
const populationFormatter = (row: number) => {
  const isUnknown = row === 0;
  const populationStyle: CSSProperties = {
    color: !isUnknown ? "black" : "red",
  };
  return <span style={populationStyle}>{isUnknown ? "UNKNOWN" : row}</span>;
};

// This is the structure for all of the different columns in the table. All of them have the same
// keys - but the values differ.
export const columnStructure: Array<Column> = [
  {
    dataField: "id",      // The name of the datafield
    text: "ID",           // What is displayed in the table
    sort: true,           // Whether or not the column is sortable
    formatter: undefined, // The formatter, if it has one 
    searchable: true,     // Whether or not the column is searchable
    filter: textFilter(), // The filter for the column.
  },
  {
    dataField: "name",
    text: "Name",
    sort: true,
    formatter: undefined,
    searchable: true,
    filter: undefined,
  },
  {
    dataField: "name_native",
    text: "English name",
    sort: true,
    formatter: undefined,
    searchable: true,
    filter: textFilter(),
  },
  {
    dataField: "country",
    text: "Country",
    sort: true,
    formatter: undefined,
    searchable: true,
    filter: textFilter(),
  },
  {
    dataField: "isCapital",
    text: "isCapital",
    sort: true,
    formatter: isCapitalFormatter,
    searchable: true,
    filter: selectFilter({ options: isCapitalSelectOptions }),
  },
  {
    dataField: "iso2",
    text: "is2",
    sort: true,
    formatter: undefined,
    searchable: true,
    filter: textFilter(),
  },
  {
    dataField: "iso3",
    text: "iso3",
    sort: true,
    formatter: undefined,
    searchable: true,
    filter: textFilter(),
  },
  {
    dataField: "population",
    text: "Population",
    sort: true,
    searchable: false,
    formatter: populationFormatter,
    filter: undefined,
  },
];

// This is the option for the pagination of the table. It decides how the pagination should function and work.
export const paginationOptions: Partial<PaginationOptions> = {
  defaultSortName: "id",
  defaultSortOrder: "asc",
  noDataText: "Loading data from API",
  prePage: "Previous",
  nextPage: "Next",
  sizePerPageList: [10, 25, 50, 100],
  showTotal: true,
  firstPageText: "First",
  prePageText: "Back",
  nextPageText: "Next",
  lastPageText: "Last",
};

/**
 * This function is called when a row is clicked. It will expand the row, show information - and a map
 * of the chosen city.
 */
export const expandRow: ExpandRowProps<any> = {
  renderer: (city: City) => {
    return (
      <div className="expanded-row">
        <div>
        <InitMap city={city}/>
        </div>
      </div>
    );
  },
};

/**
 * This is a function to delay time. It is used when logging in and registering for the user to
 * get a chance to see the feedback
 * @param ms - milliseconds to delay
 */
export const delay = (ms: number) => {
  return new Promise( resolve => setTimeout(resolve, ms) );
}
