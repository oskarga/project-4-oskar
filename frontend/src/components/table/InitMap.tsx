import React from "react";
import GoogleMapReact from "google-map-react";

/**
 * Function for inserting a pin on a google maps map.
 * @param map the map that should get a marker
 * @param center lat and lng of where the marker should be placed
 */
export const handleApiLoaded = (
  map: google.maps.Map,
  center: { lat: number; lng: number }
) => {
  new google.maps.Marker({
    position: center,
    map: map,
  });
};

interface InitMapProps {
  city: City;
}

/**
 * This component is used to show details about a city. This currently happens when a row is expanded.
 * @param city takes in a city and initiates a google maps map and shows other details
 */
const InitMap: React.FC<InitMapProps> = ({ city }) => {
  const center = {
    lat: city.lat,
    lng: city.lng,
  };
  return (
    <div className="map">
      <h3>
        <strong>{city.name}</strong> - {city.country}
      </h3>
      <div id="expanded-row-container">
        <div id="expanded-row-text">
          <p>
            <strong>English name:</strong> {city.name_native}
          </p>
          <p>
            <strong>Country:</strong> {city.country}
          </p>
          <p>
            <strong>Capital?:</strong> {city.isCapital.toString()}
          </p>
          <p>
            <strong>Population:</strong>{" "}
            {city.population === 0 ? "UNKNOWN" : city.population}
          </p>
          <p>
            <strong>Latitude:</strong> {city.lat}
          </p>
          <p>
            <strong>Longitude:</strong> {city.lng}
          </p>
        </div>
        <div className="google-map">
          <h4>Here is a map of {city.name}</h4>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyA8We1eng99-PVn09XsFpoNV5eDEtBjTBg",
            }}
            defaultCenter={center}
            defaultZoom={11}
            yesIWantToUseGoogleMapApiInternals
            onGoogleApiLoaded={({ map }) => handleApiLoaded(map, center)}
          />
        </div>
      </div>
    </div>
  );
};

export default InitMap;
