import React from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import filterFactory from "react-bootstrap-table2-filter";
import {
  columnStructure,
  paginationOptions,
  expandRow,
} from "./utils/constants";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";

interface DataTableProps {
  cities: City[];
}

/**
 * The datatable is the main component that displays the information about all the cities. It is an interactive
 * table with search and filtering. The individual rows can also be expanded to show more information about each
 * city - for example a map. It is a Bootstrap table.
 * @param cities - the cities fetched from the API 
 */
const DataTable: React.FC<DataTableProps> = ({ cities }) => {
  const { SearchBar } = Search;

  return (
    <div className="div-container">
      <ToolkitProvider
        keyField="id"
        data={cities}
        columns={columnStructure}
        search={{ searchFormatted: true }}
      >
        {(props) => (
          <div>
            <SearchBar
              placeholder="Search the table for whatever you want :-)"
              {...props.searchProps}
            />
            <BootstrapTable
              bootstrap4
              pagination={paginationFactory(paginationOptions)}
              expandRow={expandRow}
              striped
              hover
              condensed
              filterPosition="top"
              noDataIndication={
                cities.length > 0
                  ? "No city matched that query :`("
                  : "Table is empty"
              }
              filter={filterFactory()}
              {...props.baseProps}
            />
          </div>
        )}
      </ToolkitProvider>
    </div>
  );
};

export default DataTable;
