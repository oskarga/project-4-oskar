import React, { useState } from "react";
import { Formik } from "formik";
import { addNewUser, validateUser } from "../api/Controller";
import { useDispatch } from "react-redux";
import { LoggedIn } from "../actions/LoginActions";
import ProfileView from "./ProfileView";
import { delay } from "./table/utils/constants";

const initialValues = {
  username: "",
  password: "",
  age: 18,
  country: "",
  city: "",
  favourite_city: "",
  favourite_country: "",
};

interface LoginPageProps {
  loggedIn: boolean;
}

/**
 * The page displayed when the user is not viewing the datatable
 * @param loggedIn - whether or not the user is logged in
 */
const LoginPage: React.FC<LoginPageProps> = ({ loggedIn }) => {
  const dispatch = useDispatch();
  const [register, setRegister] = useState<boolean>(false);
  const [showError, setShowError] = useState<boolean>(false);
  const [showSuccess, setShowSuccess] = useState<boolean>(false);

  /**
   * This method does different thing depending on if the user is trying to login or register a new user.
   * Register:
   *          It will send a POST request to the API with the provided user, and get a response
   *          If the response is good, it will hide errors and show success feedback, and log the user into the
   *          new user.
   * Login:
   *          It will check whether the provided user exists in the API. If it does, it checks that the
   *          credentials provided is correct. If it is, it will hide errors and show success feedback
   *          and log the user in.
   * @param user - the provided user
   */
  const handleSumbit = async (user: User) => {
    if (register) {
      const response = await addNewUser(user);
      if (response) {
        setShowSuccess(true);
        setShowError(false);
        await delay(1000);
        dispatch(LoggedIn(false, user));
      } else {
        setShowSuccess(false);
        setShowError(true);
      }
    } else {
      const response = await validateUser(user);
      //Login successfull
      if (response.data.status !== 401) {
        setShowSuccess(true);
        setShowError(false);
        await delay(1000);
        dispatch(LoggedIn(true, response.data));
      } else {
        setShowSuccess(false);
        setShowError(true);
      }
    }
  };

  return (
    <div className="div-container">
      {loggedIn ? (
        <ProfileView />
      ) : (
        <div id="form-container">
          <h1>
            <strong>{register ? "Register" : "Login"}</strong>
          </h1>
          <Formik initialValues={initialValues} onSubmit={handleSumbit}>
            {({
              values,
              errors,
              touched,
              handleChange,
              handleBlur,
              handleSubmit,
              /* and other goodies */
            }) => (
              <form onSubmit={handleSubmit}>
                <label>Username * </label>

                <input
                  type="text"
                  name="username"
                  id="usernameinput"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.username}
                  placeholder="Username"
                  required
                />

                {errors.username && touched.username && errors.username}
                <label>Password * </label>

                <input
                  type="password"
                  name="password"
                  id="passwordinput"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.password}
                  placeholder="Password"
                  required
                />

                {errors.password && touched.password && errors.password}
                {register && (
                  <>
                    <label>Age</label>

                    <input
                      type="number"
                      name="age"
                      min={5}
                      max={99}
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.age}
                    />

                    <label>Country</label>

                    <input
                      type="text"
                      name="country"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Country"
                      value={values.country}
                    />

                    <label>City</label>

                    <input
                      type="text"
                      name="city"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="City"
                      value={values.city}
                    />

                    <label>Favourite country</label>

                    <input
                      type="text"
                      name="favourite_country"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Favourite country"
                      value={values.favourite_country}
                    />

                    <label>Favourite city</label>

                    <input
                      type="text"
                      name="favourite_city"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      placeholder="Favourite city"
                      value={values.favourite_city}
                    />
                  </>
                )}
                <p
                  id="register-login-feedback"
                  onClick={() => {
                    setRegister(!register);
                    setShowError(false);
                    setShowSuccess(false);
                  }}
                >
                  Click here to {register ? "log in" : "register"}
                </p>

                {showError && !showSuccess && (
                  <p style={{ color: "red" }}>
                    {!register ? "Login" : "Register"} failed | Wrong
                    credentials
                  </p>
                )}
                {showSuccess && !showError && (
                  <p style={{ color: "green" }}>
                    {!register ? "Logged in " : "Registered new user "}
                    successfully!
                  </p>
                )}

                <button type="submit" id="reg-log-btn">
                  {!register ? "Log in" : "Register new user"}
                </button>
              </form>
            )}
          </Formik>
        </div>
      )}
      <p id="register-req">
        {register
          ? "* required | must be more than 6 but no more than 30 characters"
          : ""}
      </p>
    </div>
  );
};

export default LoginPage;
