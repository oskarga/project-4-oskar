import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { LoggedOut } from "../actions/LoginActions";
import { RootStoreType } from "../store/store";
import { handleApiLoaded } from "../components/table/InitMap";
import GoogleMapReact from "google-map-react";
import { removeUser } from "../api/Controller";
import Axios from "axios";

/**
 * This component shows the information about the user
 */
const ProfileView = () => {
  const [favCity, setFavCity] = useState<City>();
  // Fetches the user and cities from the store
  const user: User = useSelector((state: RootStoreType) => state.login.user);
  const cities: City[] = useSelector(
    (state: RootStoreType) => state.datatable.cities
  );

  const dispatch = useDispatch();

  /**
   * Helper method to capitalize the first letter in a string
   * @param string - the string
   */
  const capitalizeFirstLetter = (string: string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };

  /**
   * Checks whether the favourite city the user has is in the cities table, and returns it if it is.
   * This is used to display a map of the city if it exists.
   */
  const getFavCity = () => {
    // sjekk om by eksisterer i cities
    const fav_city = user.favourite_city;
    if (fav_city === undefined) return;
    const city: City[] = cities.filter(
      (city: City) => fav_city.toLowerCase() === city.name.toLowerCase()
    );
    if (city.length > 0) {
      console.log(city[0]);
      setFavCity(city[0]);
    }
  };

  /**
   * Gets the users favourite city when the component loads
   */
  useEffect(() => {
    getFavCity();
    // eslint-disable-next-line
  }, []);

  /**
   * Logged the user out
   */
  const logout = () => {
    dispatch(LoggedOut(false, user));
  };

  const deleteUser = () => {
    removeUser(user.username);
    dispatch(LoggedOut(false, user));
  };

  return (
    <div id="profile-view-container">
      <h1>
        <strong>View your profile!</strong>
      </h1>
      <div className="profile-info">
        <div>
          <p>
            <strong>Username:</strong> {user.username}
          </p>
          <p>
            <strong>Age:</strong> {user.age}
          </p>
          <p>
            <strong>City:</strong>{" "}
            {user.city
              ? capitalizeFirstLetter(user.city)
              : "You have not told us your city"}
          </p>
          <p>
            <strong>Country:</strong>{" "}
            {user.country
              ? capitalizeFirstLetter(user.country)
              : "You have told us your country"}
          </p>
          <p>
            <strong>Your favourite city is:</strong>{" "}
            {user.favourite_city
              ? capitalizeFirstLetter(user.favourite_city)
              : "You have told us your favourite city"}
          </p>
          <p>
            <strong>Your favourite country is:</strong>{" "}
            {user.favourite_country
              ? capitalizeFirstLetter(user.favourite_country)
              : "You have told us your favourite country"}
          </p>
          <button id="logout-btn" onClick={logout}>
            Log out
          </button>
          <br></br>
          <br></br>
          <button id="delete-btn" onClick={deleteUser}>
            Delete account
          </button>
        </div>
        {favCity !== undefined && (
          <div className="google-map">
            <h4>Here is a map of your favourite city!</h4>
            <GoogleMapReact
              bootstrapURLKeys={{
                key: "AIzaSyA8We1eng99-PVn09XsFpoNV5eDEtBjTBg",
              }}
              defaultCenter={{ lat: favCity.lat, lng: favCity.lng }}
              defaultZoom={11}
              yesIWantToUseGoogleMapApiInternals
              onGoogleApiLoaded={({ map }) =>
                handleApiLoaded(map, { lat: favCity.lat, lng: favCity.lng })
              }
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default ProfileView;
