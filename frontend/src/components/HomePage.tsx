import React from "react";
import DataTable from "./table/DataTable";

interface HomePageProps {
  cities: City[];
}

/**
 * The homepage is the page that displays the datatable. It exists as a middleman between the table and
 * the App.tsx
 * @param cities - the cities loaded from API
 */
const HomePage: React.FC<HomePageProps> = ({ cities }) => {
  return (
    <div>
      <DataTable cities={cities} />
    </div>
  );
};

export default HomePage;
