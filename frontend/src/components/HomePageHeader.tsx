import React from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Login } from "../actions/LoginActions";
import { useDispatch } from "react-redux";

/**
 * Styles for the Material-ui css components.
 */
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      background: "red",
    },
    appbar: {
      backgroundColor: "#224743",
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      cursor: "pointer",
      userSelect: "none",
      "&:hover":{
        color: "lightblue"
      },
      "&:active":{
        color: "red"
      }
    },
  })
);

interface HompageHeaderProps {
  loggedIn: boolean;
  user: User;
}

/**
 * The header is displayed on all the pages, and will change depending on if the user is logged in or not.
 * @param loggedIn - whether or not the user is logged in
 * @param user - the user
 */
const HomePageHeader: React.FC<HompageHeaderProps> = ({ loggedIn, user }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const isLoggedIn = !loggedIn && user;

  /**
   * Dispatches login => true in the user changed the page. 
   * The dispatch only changes page
   */
  const handleLogin = () => {
    dispatch(Login(true, user, loggedIn));
  };

  /**
   * Dispatches login => false in the user changed the page. 
   * The dispatch only changes page
   */
  const returnToTable = () => {
    dispatch(Login(false, user, loggedIn));
  };

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appbar}>
        <Toolbar>
          <Typography
            variant="h6"
            className={classes.title}
            onClick={returnToTable}
          >
            Table containing data for cities around the world!
          </Typography>
          <Button color="inherit" onClick={handleLogin}>
            {isLoggedIn ? "Login / Register" : `Logged in as: ${user.username}`}
          </Button>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default HomePageHeader;
