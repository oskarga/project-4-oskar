import { createStore, applyMiddleware } from "redux";
import RootReducer from "./reducers/RootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from 'redux-thunk'
import { createLogger } from "redux-logger";

// Logger middleware to logg nicely to the console each time an action is dispatched
const logger = createLogger();

// The store that will be imported and used in index.tsx
const store = createStore(RootReducer, composeWithDevTools(applyMiddleware(thunk, logger)));
export type RootStoreType = ReturnType<typeof RootReducer>

export default store