import {
  CitiesDispatchTypes,
  CITIES_FAIL,
  CITIES_LOADING,
  CITIES_SUCCESS,
} from "../../actions/CitiesActionTypes";

interface DefaultStateI {
  loading: boolean;
  cities: City[];
}

const defaultState: DefaultStateI = {
  loading: false,
  cities: [],
};

/**
 * This reducer checks the types for the actions concerning fetching the cities from the API.
 * It will either be in loading, fail or succeed.
 * @param state - Current state
 * @param action - Dispatched action
 */
const CitiesReducer = (
  state: DefaultStateI = defaultState,
  action: CitiesDispatchTypes
): DefaultStateI => {
  switch (action.type) {
    case CITIES_LOADING:
      return { loading: true, cities: [] };
    case CITIES_FAIL:
      return { loading: false, cities: [] };
    case CITIES_SUCCESS:
      return { loading: false, cities: action.payload };
    default:
      return state;
  }
};

export default CitiesReducer;
