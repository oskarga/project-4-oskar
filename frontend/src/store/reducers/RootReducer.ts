
import {combineReducers} from 'redux'
import CitiesReducer from './CitiesReducer';
import LoginReducer from './LoginReducer'

/**
 * Combines the reducers into one reducer
 */
const RootReducer = combineReducers({
    datatable: CitiesReducer,
    login: LoginReducer
})

export default RootReducer;