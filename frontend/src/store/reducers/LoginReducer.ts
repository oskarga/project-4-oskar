import {
  LoginActionTypes,
  LOGIN_FALSE,
  LOGIN_TRUE,
  LOGGED_IN_TRUE,
  LOGGED_IN_FALSE,
} from "../../actions/LoginActionsTypes";

interface DefaultStateI {
  login: boolean;
  loggedIn: boolean;
  user: User;
}

// This is actually redundant, but it is funny. I'll leave it in
const defaultUser: User = {
  username: "Anonym frosk",
  password: "notyetdecided",
  age: 18,
  city: "Wondertown",
  country: "Wonderland",
  favourite_city: "Minas Tirith",
  favourite_country: "Gondor"
}

const defaultState: DefaultStateI = {
  login: false, // Whether to display the loginpage or not
  loggedIn: false, // Whether or not the user is logged in as a user
  user: defaultUser, // Funny addition, but absolutely redundant
};

/**
 * This reducer handles the actions for changing page to the login page, 
 * and the login/registering of the user.
 * @param state 
 * @param action 
 */
const LoginReducer = (
  state: DefaultStateI = defaultState,
  action: LoginActionTypes
): DefaultStateI => {
  switch (action.type) {
    case LOGIN_TRUE:
      return { loggedIn: action.loggedIn, login: action.login, user: action.user };
    case LOGIN_FALSE:
      return { loggedIn: action.loggedIn, login: action.login, user: action.user };
    case LOGGED_IN_FALSE:
      return { loggedIn: action.loggedIn, login: action.login, user: action.user };
    case LOGGED_IN_TRUE:
      return { loggedIn: action.loggedIn, login: action.login, user: action.user};
    default:
      return state;
  }
};

export default LoginReducer;
