import React, { useEffect, useState } from "react";
import HomePage from "./components/HomePage";
import { useDispatch, useSelector } from "react-redux";
import { RootStoreType } from "./store/store";
import { LoadAllCities } from "./actions/CitiesActions";
import HomePageHeader from "./components/HomePageHeader";
import LoginPage from "./components/LoginPage";
import "./App.css";

/**
 * Main app component for the page
 */
function App() {
  const dispatch = useDispatch();
  const [cities, setCities] = useState<City[]>([]);

  // Fetches the data needed from the store
  const datatableState = useSelector((state: RootStoreType) => state.datatable);
  const loginState = useSelector((state: RootStoreType) => state.login);

  /**
   * Starts loading all the pages from the API with a dispatch call
   */
  useEffect(() => {
    dispatch(LoadAllCities());
  }, [dispatch]);

  /**
   * When the cities are not loading anymore, it will set the cities-state to the fetched cities
   */
  useEffect(() => {
    if (datatableState.cities) {
      setCities(datatableState.cities);
    }
  }, [datatableState.cities]);

  return (
    <div>
      <HomePageHeader loggedIn={loginState.loggedIn} user={loginState.user} />
      {!loginState.login && <HomePage cities={cities} />}
      {loginState.login && <LoginPage loggedIn={loginState.loggedIn} />}
    </div>
  );
}

export default App;
