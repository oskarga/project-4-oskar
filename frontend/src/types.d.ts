interface City {
  name: string;
  name_native: string;
  country: string;
  isCapital: boolean;
  iso2: string;
  iso3: string;
  population: number;
  lat: number;
  lng: number;
  id: number;
}

interface Column {
  dataField: string;
  text: string;
  sort: boolean;
  formatter: any | undefined;
  filter: Partial<any> | undefined;
  searchable: boolean;
}

interface PaginationOptions {
  defaultSortName: string;
  defaultSortOrder: string;
  noDataText: string;
  prePage: string;
  nextPage: string;
  sizePerPageList: Array<number>;
  showTotal: boolean;
  firstPageText: string;
  prePageText: string;
  nextPageText: string;
  lastPageText: string;
}

interface User {
  username: string;
  password: string;
  age: number;
  country: string;
  city: string;
  favourite_city: string;
  favourite_country: string;
}
