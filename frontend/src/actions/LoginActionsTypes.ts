export const LOGIN_TRUE = "LOGIN_TRUE";
export const LOGIN_FALSE = "LOGIN_FALSE";
export const LOGGED_IN_TRUE = "LOGGED_IN_TRUE";
export const LOGGED_IN_FALSE = "LOGGED_IN_FALSE";

export interface LoginTrue {
  type: typeof LOGIN_TRUE;
  user: User;
  login: boolean;
  loggedIn: boolean;

}

export interface LoginFalse {
  type: typeof LOGIN_FALSE;
  user: User;
  login: boolean;
  loggedIn: boolean;
}

export interface LoggedInFalse {
  type: typeof LOGGED_IN_FALSE;
  user: User;
  login: boolean;
  loggedIn: boolean;
}

export interface LoggedInTrue {
  type: typeof LOGGED_IN_TRUE;
  user: User;
  login: boolean;
  loggedIn: boolean;
}

// The types can be either one of these four
export type LoginActionTypes =
  | LoginTrue
  | LoginFalse
  | LoggedInTrue
  | LoggedInFalse
