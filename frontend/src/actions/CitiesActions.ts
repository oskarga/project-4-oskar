import { Dispatch } from "redux";
import {
  CitiesDispatchTypes,
  CITIES_FAIL,
  CITIES_LOADING,
  CITIES_SUCCESS,
} from "./CitiesActionTypes";
import { fetchAllCities } from "../api/Controller";

/**
 * This action will fetch the cities from the API. It will either result in a SUCCESS or a FAIL,
 * but will always dispatch a LOADING at the start
 */
export const LoadAllCities = () => async (
  dispatch: Dispatch<CitiesDispatchTypes>
) => {
  try {
    dispatch({
      type: CITIES_LOADING,
    });
    const cities: City[] = await fetchAllCities();
    // When the loading was successful, we dispatch a SUCCESS with the cities as the payload
    dispatch({ type: CITIES_SUCCESS, payload: cities });
  } catch (e) {
    // If an exception happened, dispatch a FAIL
    dispatch({ type: CITIES_FAIL });
  }
};
