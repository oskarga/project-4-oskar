export const CITIES_LOADING = "CITIES_LOADING";
export const CITIES_FAIL = "CITIES_FAIL";
export const CITIES_SUCCESS = "CITIES_SUCCESS";

export interface CitiesLoading {
  type: typeof CITIES_LOADING;
}

export interface CitiesFail {
  type: typeof CITIES_FAIL;
}

// Only the SUCCESS needs payload
export interface CitiesSuccess {
  type: typeof CITIES_SUCCESS;
  payload: City[];
}

// The types can be either one of these three
export type CitiesDispatchTypes = CitiesLoading | CitiesFail | CitiesSuccess;
