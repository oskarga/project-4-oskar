import { Dispatch } from "redux";
import {
  LOGGED_IN_FALSE,
  LOGGED_IN_TRUE,
  LoginActionTypes,
  LOGIN_FALSE,
  LOGIN_TRUE,
} from "./LoginActionsTypes";

/**
 * This action will dispatch either TRUE or FALSE depending on the given parameters. It will change
 * the page to and from the login page
 * @param login - whether or not login should be true
 * @param user - the user 
 * @param isLoggedIn - whether or not the user should be logged in after the dispatch
 */
export const Login = (
  login: boolean,
  user: User,
  isLoggedIn: boolean
) => async (dispatch: Dispatch<LoginActionTypes>) => {
  login
    ? dispatch({
        type: LOGIN_TRUE,
        login: login,
        user: user,
        loggedIn: isLoggedIn,
      })
    : dispatch({
        type: LOGIN_FALSE,
        login: login,
        user: user,
        loggedIn: isLoggedIn,
      });
};

/**
 * This action will change the users logged-in state to TRUE or FALSE.
 * @param login - whether or not logged in should be true 
 * @param user - the user
 */
export const LoggedIn = (login: boolean, user: User) => async (
  dispatch: Dispatch<LoginActionTypes>
) => {
  // Logged the user in as the given user
  dispatch({ type: LOGGED_IN_TRUE, login: login, user: user, loggedIn: true });
};

export const LoggedOut = (login: boolean, user: User) => async (
  dispatch: Dispatch<LoginActionTypes>
) => {
  // The user failed the login
  dispatch({ type: LOGGED_IN_FALSE, login: login, user: user, loggedIn: false });
};

