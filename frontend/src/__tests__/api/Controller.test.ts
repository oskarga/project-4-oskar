import {
  addNewUser,
  fetchAllCities,
  fetchSortByCityName,
  fetchSortByCountry,
  removeUser,
  validateUser,
} from "../../api/Controller";

export const testUser_fail: User = {
  age: 20,
  city: "",
  country: "",
  favourite_city: "",
  favourite_country: "",
  password: "non-existing-password",
  username: "non-existing-username",
};

export const testUser_correct: User = {
  age: 20,
  city: "Trondheim",
  country: "Norway",
  favourite_city: "Tokyo",
  favourite_country: "Japan",
  password: "testpassword",
  username: "testuser",
};

describe("Controller.ts testing", () => {
  it("Test: fetchAllCities", async () => {
    const response = await fetchAllCities();
    expect(response).toBeDefined();
    expect(response.length).toBeGreaterThan(0);
  });

  it("Test: fetchSortByCountry | Correct value", async () => {
    const response = await fetchSortByCountry("Japan");
    expect(response).toBeDefined();
    expect(response.length).toBeGreaterThan(0);
  });

  it("Test: fetchSortByCountry | Incorrect value", async () => {
    const response = await fetchSortByCountry("");
    expect(response).toBeDefined();
    expect(response.length).toBe(0);
  });

  it("Test: fetchSortByCityName | Correct value", async () => {
    const response = await fetchSortByCityName("London");
    expect(response).toBeDefined();
    expect(response.length).toBeGreaterThan(0);
  });

  it("Test: fetchSortByCityName | Incorrect value", async () => {
    const response = await fetchSortByCityName("");
    expect(response).toBeDefined();
    expect(response.length).toBe(0);
  });

  it("Test: validateUser | Correct value", async () => {
    await addNewUser(testUser_correct);
    const response = await validateUser(testUser_correct);
    expect(response.status).toBe(200);
  });

  it("Test: validateUser | Incorrect value", async () => {
    const response = await validateUser(testUser_fail);
    expect(response.data.status).toBe(401);
  });

  it("Test: addNewUser | Correct value", async () => {
    await removeUser(testUser_correct.username);
    const response = await addNewUser(testUser_correct);
    expect(response).toBeDefined();
  });
});
