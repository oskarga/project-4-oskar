import React from "react";
import { Provider } from "react-redux";
import App from "../App";
import store from "../store/store";
import renderer from 'react-test-renderer';
import { mount } from "../__test-setup__/setup";

describe("App.tsx testing (<App />)", () => {
  it("App renders correctly (snapshot)", () => {
    const wrapper = mount(
      <Provider store={store}>
        <App />
      </Provider>
    );
    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("App renders correctly when login = true (snapshot)", () => {
    const newstore = store;
    newstore.getState().login.login = true;
    const wrapper = renderer.create(
      <Provider store={newstore}>
        <App />
      </Provider>
    );
    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });
});
