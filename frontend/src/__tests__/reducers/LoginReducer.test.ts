import { LOGGED_IN_FALSE, LOGGED_IN_TRUE, LOGIN_FALSE, LOGIN_TRUE } from "../../actions/LoginActionsTypes";
import reducer from "../../store/reducers/LoginReducer";
import { testUser_correct } from "../api/Controller.test";

const loginTrueState = { loggedIn: false, user: testUser_correct, login: true};
const loginFalseState = { loggedIn: false, user: testUser_correct, login: false};
const loggedInTrueState = { loggedIn: true, user: testUser_correct, login: false};
const loggedInFalseState = { loggedIn: false, user: testUser_correct, login: true};

describe("Tests for the LoginReducer used in the app", () => {
  it("Test: LoginReducer | LOGIN_TRUE", () => {
    expect(
      reducer(undefined, {
        type: LOGIN_TRUE,
        loggedIn: false,
        user: testUser_correct,
        login: true
      })
    ).toEqual(loginTrueState);
  });

  it("Test: LoginReducer | LOGIN_FALSE", () => {
    expect(
      reducer(undefined, {
        type: LOGIN_FALSE,
        loggedIn: false,
        user: testUser_correct,
        login: false
      })
    ).toEqual(loginFalseState);
  });

  it("Test: LoginReducer | LOGGED_IN_TRUE", () => {
    expect(
      reducer(undefined, {
        type: LOGGED_IN_TRUE,
        loggedIn: true,
        user: testUser_correct,
        login: false
      })
    ).toEqual(loggedInTrueState);
  });

  it("Test: LoginReducer | LOGGED_IN_FALSE", () => {
    expect(
      reducer(undefined, {
        type: LOGGED_IN_FALSE,
        loggedIn: false,
        user: testUser_correct,
        login: true
      })
    ).toEqual(loggedInFalseState);
  });
});