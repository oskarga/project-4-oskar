import {
  CITIES_FAIL,
  CITIES_LOADING,
  CITIES_SUCCESS,
} from "../../actions/CitiesActionTypes";
import reducer from "../../store/reducers/CitiesReducer";

const cities = [
  {
    name: "Tokyo",
    name_native: "Tokyo",
    lat: 35.685,
    lng: 139.7514,
    country: "Japan",
    iso2: "JP",
    iso3: "JPN",
    isCapital: true,
    population: 35676000,
    id: 1,
  },
  {
    name: "New York",
    name_native: "New York",
    lat: 40.6943,
    lng: -73.9249,
    country: "United States",
    iso2: "US",
    iso3: "USA",
    isCapital: false,
    population: 19354922,
    id: 2,
  },
];

const loadingState = { cities: [], loading: true };
const failState = { cities: [], loading: false };
const successState = { cities: cities, loading: false };

describe("Tests for CitiesReducer used in the app", () => {
  it("Test: CitiesReducer | CITIES_LOADING", () => {
    expect(
      reducer(undefined, {
        type: CITIES_LOADING,
      })
    ).toEqual(loadingState);
  });

  it("Test: CitiesReducer |  CITIES_FAIL", () => {
    expect(
      reducer(undefined, {
        type: CITIES_FAIL,
      })
    ).toEqual(failState);
  });

  it("Test: CitiesReducer |  CITIES_SUCCESS", () => {
    expect(
      reducer(undefined, {
        type: CITIES_SUCCESS,
        payload: cities
      })
    ).toEqual(successState);
  });
});
