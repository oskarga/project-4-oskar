import React from "react";
import renderer from "react-test-renderer";
import { testUser_correct } from "../api/Controller.test";
import store from "../../store/store";
import { Provider } from "react-redux";
import { fireEvent, render } from "@testing-library/react";
import LoginPage from "../../components/LoginPage";
import { removeUser } from "../../api/Controller";
import { mount } from "../../__test-setup__/setup";

describe("LoginPage.tsx testing (<LoginPage /> with props)", () => {
  beforeAll(() => {
    removeUser(testUser_correct.username);
  });

  it("Test: LoginPage renders correctly (snapshot) with loggedIn = true", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <LoginPage loggedIn={true} />
      </Provider>
    );

    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("Test: LoginPage renders correctly (snapshot) with loggedIn = false", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <LoginPage loggedIn={false} />
      </Provider>
    );

    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("Test: LoginPage toggle to register and login works", () => {
    const { getByText } = render(
      <Provider store={store}>
        <LoginPage loggedIn={false} />
      </Provider>
    );

    // Register toggle
    let toggleRegisterLogin = getByText("Click here to register");
    expect(toggleRegisterLogin).toBeDefined();
    fireEvent.click(toggleRegisterLogin);

    // Login toggle
    toggleRegisterLogin = getByText("Click here to log in");
    expect(toggleRegisterLogin).toBeDefined();
  });

  it("Test: LoginPage invalid register fail", async () => {
    const { getByText, getByPlaceholderText } = render(
      <Provider store={store}>
        <LoginPage loggedIn={false} />
      </Provider>
    );

    // Register toggle
    const toggleRegisterLogin = getByText("Click here to register");
    expect(toggleRegisterLogin).toBeDefined();
    fireEvent.click(toggleRegisterLogin);

    const registerButton = getByText("Register new user");
    // Trying to register with too short username
    const usernameInput = getByPlaceholderText("Username");
    const passwordInput = getByPlaceholderText("Password");

    fireEvent.change(usernameInput, { target: { value: "test" } });
    fireEvent.change(passwordInput, { target: { value: "testpassword" } });
    fireEvent.click(registerButton);

    expect(() => {
      getByText("Registered new user successfully");
    }).toThrow();

    fireEvent.change(usernameInput, { target: { value: "testuseroklmao" } });
    fireEvent.change(passwordInput, { target: { value: "test" } });
    fireEvent.click(registerButton);

    expect(() => {
      getByText("Registered new user successfully");
    }).toThrow();
  });

  it("Test: LoginPage register success", async () => {
    const component = mount(
      <Provider store={store}>
        <LoginPage loggedIn={false} />
      </Provider>
    );
    await removeUser("testuserlmao");
    const toggleRegisterLogin = component.find("#register-login-feedback");
    expect(toggleRegisterLogin).toBeDefined();
    toggleRegisterLogin.simulate("click");

    const username = component.find("#usernameinput")
    const password = component.find("#passwordinput")
    username.simulate("change", {target: {value: "testuserlmao"}})
    password.simulate("change", {target: {value: "testuserlmaopassword"}})

    const form = component.find("form");
    form.simulate("submit", { preventDefault: jest.fn() });
  });

  it("Test: LoginPage login fail", async () => {
    const component = mount(
      <Provider store={store}>
        <LoginPage loggedIn={false} />
      </Provider>
    );

    const form = component.find("form");
    const username = component.find("#usernameinput")
    const password = component.find("#passwordinput")
    username.simulate("change", {target: {value: ""}})
    password.simulate("change", {target: {value: ""}})
    form.simulate("submit", { preventDefault: jest.fn() });
  });

  it("Test: LoginPage login success", async () => {
    const component = mount(
      <Provider store={store}>
        <LoginPage loggedIn={false} />
      </Provider>
    );

    const username = component.find("#usernameinput")
    const password = component.find("#passwordinput")
    username.simulate("change", {target: {value: "testuserlmao"}})
    password.simulate("change", {target: {value: "testuserlmaopassword"}})
    const form = component.find("form");
    form.simulate("submit", { preventDefault: jest.fn() });
  });
});
