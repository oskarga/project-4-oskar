import React from "react";
import { Provider } from "react-redux";
import ProfileView from "../../components/ProfileView";
import renderer from "react-test-renderer";
import store from "../../store/store";
import { testUser_correct, testUser_fail } from "../api/Controller.test";
import { fireEvent, render } from "@testing-library/react";

describe("ProfileView.tsx testing (<ProfileView />)", () => {
  it("Test: ProfileView renders correctly (snapshot)", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <ProfileView />
      </Provider>
    );

    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("Test: ProfileView views information correct WITH MAP", () => {
    const newstore = store;
    newstore.getState().login.user = testUser_correct;
    newstore.getState().datatable.cities = [
      {
        name: "Tokyo",
        name_native: "Tokyo",
        lat: 35.685,
        lng: 139.7514,
        country: "Japan",
        iso2: "JP",
        iso3: "JPN",
        isCapital: true,
        population: 35676000,
        id: 1,
      },
    ];
    const { getByText } = render(
      <Provider store={newstore}>
        <ProfileView />
      </Provider>
    );

    const usernamefield = getByText(`${testUser_correct.username}`);
    const agefield = getByText(`${testUser_correct.age}`);
    const cityfield = getByText(`${testUser_correct.city}`);
    const countryfield = getByText(`${testUser_correct.country}`);
    const favcityfield = getByText(`${testUser_correct.favourite_city}`);
    const favcountryfield = getByText(`${testUser_correct.favourite_country}`);
    expect(usernamefield).toBeDefined();
    expect(agefield).toBeDefined();
    expect(cityfield).toBeDefined();
    expect(countryfield).toBeDefined();
    expect(favcityfield).toBeDefined();
    expect(favcountryfield).toBeDefined();
  });

  it("Test: ProfileView views information correct WITHOUT MAP", () => {
    const newstore = store;
    newstore.getState().login.user = testUser_fail;
    const { getByText } = render(
      <Provider store={newstore}>
        <ProfileView />
      </Provider>
    );

    const usernamefield = getByText(`${testUser_fail.username}`);
    const agefield = getByText(`${testUser_fail.age}`);
    expect(() => {getByText(`${testUser_fail.city}`)});
    expect(() => {getByText(`${testUser_fail.country}`)});
    expect(() => {getByText(`${testUser_fail.favourite_city}`)});
    expect(() => {getByText(`${testUser_fail.favourite_country}`)});
    expect(usernamefield).toBeDefined();
    expect(agefield).toBeDefined();

  });

  it("Test: ProfileView logout successful", () => {
    const newstore = store;
    newstore.getState().login.user = testUser_correct;
    const { getByText } = render(
      <Provider store={newstore}>
        <ProfileView />
      </Provider>
    );

    const logout = getByText("Log out");
    fireEvent.click(logout);
    expect(store.getState().login.login).toBe(false);
  });
});
