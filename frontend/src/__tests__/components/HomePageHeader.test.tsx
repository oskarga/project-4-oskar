import React from "react";
import HomePageHeader from "../../components/HomePageHeader";
import renderer from "react-test-renderer";
import { testUser_correct } from "../api/Controller.test";
import store from "../../store/store";
import { Provider } from "react-redux";
import { fireEvent, render } from "@testing-library/react";

describe("HomePageHeader.tsx testing (<HomePageHeader /> with props)", () => {
  it("Test: HomePageHeader renders correctly (snapshot) with loggedIn = true", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <HomePageHeader loggedIn={true} user={testUser_correct} />
      </Provider>
    );

    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("Test: HomePageHeader renders correctly (snapshot) with loggedIn = false", () => {
    const wrapper = renderer.create(
      <Provider store={store}>
        <HomePageHeader loggedIn={false} user={testUser_correct} />
      </Provider>
    );

    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("Test: HomePageHeader displays login button correct with loggedIn = true", () => {
    const {getByText} = render(
      <Provider store={store}>
        <HomePageHeader loggedIn={true} user={testUser_correct} />
      </Provider>
    );
    const button = getByText(`Logged in as: ${testUser_correct.username}`)
    expect(button).toBeDefined();
    fireEvent.click(button)
    expect(store.getState().login.login).toBe(true)
  });

  it("Test: HomePageHeader displays login button correct with loggedIn = false", () => {
    const {getByText} = render(
      <Provider store={store}>
        <HomePageHeader loggedIn={false} user={testUser_correct} />
      </Provider>
    );
    const button = getByText(`Login / Register`)
    expect(button).toBeDefined();
    fireEvent.click(button)
    expect(store.getState().login.login).toBe(true)
  });

  it("Test: HomePageHeader fires dispatch to return to datatable view", () => {
    const {getByText} = render(
      <Provider store={store}>
        <HomePageHeader loggedIn={false} user={testUser_correct} />
      </Provider>
    );
    const header = getByText("Table containing data for cities around the world!")
    expect(header).toBeDefined();
    fireEvent.click(header)
    expect(store.getState().login.login).toBe(false)
  });
});
