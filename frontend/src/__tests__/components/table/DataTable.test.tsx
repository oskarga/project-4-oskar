import React from "react";
import { Provider } from "react-redux";
import DataTable from "../../../components/table/DataTable";
import store from "../../../store/store";
import renderer from "react-test-renderer";
import { fireEvent, render } from "@testing-library/react";
import { delay } from "q";

describe("DataTable.tsx testing (<DataTable />) with props", () => {
  it("Test: ProfileView renders correctly (snapshot)", () => {
    const wrapper = render(<DataTable cities={[]} />);

    // console.log('wrapper is', wrapper.debug());
    expect(wrapper).toBeDefined();
    expect(wrapper).toMatchSnapshot();
  });

  it("Test: ProfileView renders correctly when cities array is empty", () => {
    const { getByText } = render(<DataTable cities={[]} />);

    const empty = getByText("Table is empty");
    // console.log('wrapper is', wrapper.debug());
    expect(empty).toBeDefined();
  });

  it("Test: ProfileView renders correctly when cities arraay is not empty", async () => {
    const wrapper = render(
      <DataTable
        cities={[
          {
            name: "Tokyo",
            name_native: "Tokyo",
            lat: 35.685,
            lng: 139.7514,
            country: "Japan",
            iso2: "JP",
            iso3: "JPN",
            isCapital: true,
            population: 35676000,
            id: 1,
          },
          {
            name: "New York",
            name_native: "New York",
            lat: 40.6943,
            lng: -73.9249,
            country: "United States",
            iso2: "US",
            iso3: "USA",
            isCapital: false,
            population: 0,
            id: 2,
          },
        ]}
      />
    );

    const cities = await wrapper.findAllByText("Tokyo")
    const clickable = cities[0]
    // console.log('wrapper is', wrapper.debug());
    expect(clickable).toBeDefined();
    await delay(1000)
    fireEvent.click(clickable)
  });
});
