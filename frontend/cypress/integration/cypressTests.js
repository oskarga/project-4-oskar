//Testuser username: "testuser123"
//Testuser password: "testpass123"

const { wait } = require("@testing-library/react")
const { verify } = require("crypto")

describe('Login as test user', () => {
  it('Logs in as an already registered testuser.', () => {
    cy.visit("localhost:3000") //navigate to the webpage
    cy.contains("Login / Register").click() //open menu to login
    //enters login info of test user and logs in
    cy.get('input:first').type("testuser123")
    cy.get('input:last').type("testpass123")
    cy.contains("Log in").click()
    cy.contains("Logged in as: testuser123")
  })
})

describe('Get London through search', () => {
  it('Searches for London in the database.', () => {
    cy.visit("localhost:3000") //navigate to the webpage
    cy.wait(2000) // waits 2 seconds before typing into the input field. This lets the page load properly.
    cy.get('input:first').type("London")
    cy.wait(2000) // waits 2 seconds to let query complete and results render
    cy.contains("United Kingdom").click()
    cy.contains("London - United Kingdom")
  })
})

describe('Get Rome through filters', () => {
  it('Searches for Rome by applying filters.', () => {
    cy.visit("localhost:3000") //navigate to the webpage
    cy.wait(2000) // waits 2 seconds before typing into the input field. This lets the page load properly.
    cy.get("select").select("true")
    cy.get('input[id="text-filter-column-iso2"]').type("IT")
    cy.get('input[id="text-filter-column-iso3"]').type("ITA")
    cy.wait(2000)
    cy.contains("Rome").click()
    cy.contains("Rome - Italy")
  })
})

//creates a random username and password to register new user
const uid = () => Cypress._.random(0, 1e6)
const id = uid()
const testname = `testname${id}`
const password = `password${id}`

describe('Register new test user', () => {
  it('Logs in as an already registered testuser.', () => {
    cy.visit("localhost:3000") //navigate to the webpage
    cy.contains("Login / Register").click() //open menu to login
    //enters login info of test user and logs in
    cy.contains("Click here to register").click()
    cy.get('input:first').type(testname)
    cy.get('input[id ="passwordinput"]').type(password)
    cy.contains("Register new user").click()
    cy.contains("Logged in as: " + testname) 
  })
})
